package com.example.shubh.flashlight;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.security.Policy;

public class MainActivity extends Activity {

    CameraManager cm,cm2;
    Boolean isFlash,isFlashOn,Flash,FFlash;
    String camId, CAMERA_FRONT;
    Switch sw,sw1;
    TextView tv,tv1,tv2;
    int cameraId = -1;

    int val = 0;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //hiding action bar of the application.
        //getSupportActionBar().hide();

        setContentView(R.layout.activity_main);

        FFlash = false;
        isFlash = false;
        isFlashOn = false;
        sw = findViewById(R.id.switch1);
        sw1 = findViewById(R.id.switch2);
        tv = findViewById(R.id.textView);
        tv1 = findViewById(R.id.textView2);
        tv2 = findViewById(R.id.textView3);

        //--------------------check if flashlight is available or not.--------------------
        isFlash = getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        if (!isFlash) {
            //if does not supports flash feature;
            AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
            adb.setTitle("ERROR...");
            adb.setMessage("Your Device do not contain Flashlight");
            adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    onDestroy();
                }
            });
            adb.show();
            return;
        }

        //--------------------Getting front CAMERA services--------------------

        // get the number of cameras
        int numberOfCameras = Camera.getNumberOfCameras();
        // for every camera check
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            Toast.makeText(this, "inside for "+cameraId, Toast.LENGTH_SHORT).show();

            // Search for the front facing camera
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                //CAMERA_FRONT = ""+cameraId;
                Toast.makeText(this, "inside if"+cameraId, Toast.LENGTH_SHORT).show();
                break;
            }
        }
        CAMERA_FRONT = ""+cameraId;

        if (numberOfCameras == 2){
            cm2 = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        }
        else {
            sw1.setVisibility(View.INVISIBLE);
            tv1.setVisibility(View.INVISIBLE);
            tv2.setText("No Front Flash");

        }
        //return cameraId;


        //--------------------Getting CAMERA services--------------------
        cm = (CameraManager) getSystemService(Context.CAMERA_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                camId = cm.getCameraIdList()[0];
                if(numberOfCameras == 2)
                    CAMERA_FRONT = cm2.getCameraIdList()[cameraId];
                Toast.makeText(this, "back id "+camId, Toast.LENGTH_SHORT).show();

            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }


        //--------------------turning flash light on and off--------------------
        sw.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if(isFlashOn==false){
                    tv.setText("OFF");
                    isFlashOn=true;
                    Flash = false;
                    BackFlashOn();
                }
                else {
                    tv.setText("ON");
                    isFlashOn=false;
                    Flash = true;
                    BackFlashOff();
                }
            }
        });

        sw1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i;
                if(!FFlash){
                    i = 0;
                    FrontFlashon(i);
                    FFlash = true;
                    tv1.setText("OFF");
                }
                else {
                    i = 1;
                    FrontFlashon(i);
                    FFlash = false;
                    tv1.setText("ON");
                }
            }
        });
    }

    private void FrontFlashon(int i) {
        int x = i;
        if(x==0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                try {
                    cm2.setTorchMode(CAMERA_FRONT,true);
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        else if(x==1){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                try {
                    cm2.setTorchMode(CAMERA_FRONT,false);
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    //--------------------Turn flash ON--------------------
    private void BackFlashOn() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                cm.setTorchMode(camId,true);

            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
    }

    //--------------------Turn flash OFF--------------------
    private void BackFlashOff() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                cm.setTorchMode(camId,false);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
    }

    //--------------------When Back Button Pressed--------------------
    @Override
    public void onBackPressed() {
        if(val == 0) {
            final AlertDialog.Builder ab = new AlertDialog.Builder(MainActivity.this);
            ab.setTitle("Confirm");
            ab.setMessage("Are you sure you want to exit");
            ab.setCancelable(false);
            ab.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    val = 1;
                    onBackPressed();
                }
            });
            ab.setNegativeButton("Cancel", null);
            ab.show();
        }
        else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
